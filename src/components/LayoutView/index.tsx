import Taro from '@tarojs/taro'
import { View, ScrollView } from '@tarojs/components'

export default class LayoutView extends Taro.Component<any, any> {
    render() {
      const { disableScroll } = this.props
      return (
        <View>
          {
            disableScroll ? (
              <View>{this.props.children}</View>
            ) : (
              <ScrollView>
                {this.props.children}
              </ScrollView>
            )
          }
        </View>
      )
    }
}