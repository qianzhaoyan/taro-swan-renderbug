import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './index.css'
import LayoutView from '../../components/LayoutView'
import RadioBox from '../../components/RadioBox'

export default class Index extends Component<any, any> {

  constructor(props) {
    super(props)
    this.state = {
      value: '',
      options: [1,2,3]
    }
  }

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  handleChange(value) {
    this.setState({value})
  }

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '首页'
  }

  render () {
    const { options, value } = this.state
    console.log("optionsByParent==", options)
    return (
      <View className='index'>
        <LayoutView disableScroll>
          <RadioBox value={value} options={options} onChange={this.handleChange.bind(this)}></RadioBox>
        </LayoutView>
      </View>
    )
  }
}
