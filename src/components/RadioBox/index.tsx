import Taro from '@tarojs/taro'
import { View } from '@tarojs/components'
import './index.css'

export default class CheckRadio extends Taro.Component<any, any> {
  static defaultProps = {
    options: [],
  }

  componentWillMount() {
    console.log('RadioBox is componentWillmount')
  }

  componentWillUnmount() {
    console.log('RadioBox is componentWillUnmount')
  }

  handleClick = (value) => {
    this.props.onChange && this.props.onChange(value)
  }

  render() {
    const { value, options } = this.props
    console.log("optionsByRadioBox==", options)
    return options && !!options.length && <View>
      {
        options.map(item => {
          const isSelect = value == item
          return <View className={isSelect ? 'checkbox--active' : ''} onClick={this.handleClick.bind(this, item)}>{item}</View>
        })
      }
    </View>
  }
}